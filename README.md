# Dockerfiles

This is where I keep my custom Dockerfiles for various services. They're designed for use in my own network, so your mileage may vary if you use them yourself.

I use these files within Portainer; the Dockerfiles are used to create a custom image and then the docker-compose.yml is used to create a stack.

Files are named in accordance to their tag:

```
container/tag.dockerfile
container/tag.yml
container/tag.*
```
